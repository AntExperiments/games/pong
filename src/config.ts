export const backgroundColor = "#333"
export const foregroundColor = "#aaa"
export const targetFPS = 60
export const movementPerSecond = 35
export const paddleSensitivity = 10