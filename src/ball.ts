import { foregroundColor, movementPerSecond, targetFPS } from './config'

const ballDefaults = {
    size: 25
}

class Ball {
    canvas: HTMLCanvasElement
    context: CanvasRenderingContext2D

    _x: number
    get x()       { return this._x }
    set x(value)  { this._x = (value <= 100) ? (value <= 0) ? 0 : value : 100 }

    _y: number
    get y()       { return this._y }
    set y(value)  { this._y = (value <= 100) ? (value <= 0) ? 0 : value : 100 }

    get edgeX() { return this.canvas.width * (this.x / 100) - ballDefaults.size }
    get centerY() { return this.y - ballDefaults.size/200 }

    xVelocity
    yVelocity

    constructor({canvas, context}) {
        this.canvas = canvas;
        this.context = context;
        
        this._x = 50;
        this._y = 50;

        this.xVelocity = -30
        this.yVelocity = 5
    }

    physics() {
        this.x += this.xVelocity / targetFPS
        this.y += this.yVelocity / targetFPS
    }

    draw() {
        const c = this.context

        const xPosition = this.canvas.width * (this.x / 100) - ballDefaults.size/2;
        const yPosition = this.canvas.height * (this.y / 100) - ballDefaults.size/2;

        c.beginPath();
        c.arc(xPosition, yPosition, ballDefaults.size, 0, 2 * Math.PI);
        c.fillStyle = foregroundColor;
        c.fill();
    }
}

export default Ball