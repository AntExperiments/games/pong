import { foregroundColor } from './config'

const playerDefaults = {
    height: 200,
    width: 30
}

class Paddle {
    canvas: HTMLCanvasElement
    context: CanvasRenderingContext2D
    isBot: boolean

    _y: number
    get y()      { return this._y }
    set y(value) { this._y = (value <= 100) ? (value <= 0) ? 0 : value : 100 }

    get edgeX() { return (20 + playerDefaults.width) / 100 * this.canvas.width }
    get edgeY() { return this.y - playerDefaults.height/200 }

    constructor({canvas, context}, isBot?) {
        this.canvas = canvas;
        this.context = context;
        this.isBot = isBot;

        this._y = 0;
    }

    draw() {
        const c = this.context

        const offsetTopBottom = playerDefaults.height / 2 + 20
        const availableHeight = this.canvas.height - offsetTopBottom * 2
        
        const offsetLeftRight = this.isBot ? this.canvas.width - 20 - playerDefaults.width : 20

        let yPosition = availableHeight * (this.y / 100) + 20;

        c.beginPath();
        c.rect(offsetLeftRight, yPosition, playerDefaults.width, playerDefaults.height);
        c.fillStyle = foregroundColor;
        c.fill();
    }
}

export default Paddle