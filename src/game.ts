import { backgroundColor, targetFPS, paddleSensitivity, movementPerSecond } from './config'
import Paddle from './player'
import Ball from './ball'

class GameController {
    canvas: HTMLCanvasElement
    context: CanvasRenderingContext2D

    player: Paddle
    bot: Paddle
    ball: Ball

    constructor(canvas: HTMLCanvasElement) {
        this.canvas  = canvas
        this.context = canvas.getContext('2d')

        this.player  = new Paddle(this)
        this.bot     = new Paddle(this, true)
        this.ball     = new Ball(this)

        setInterval(() => { this.physics() }, 1000 / targetFPS)

        document.addEventListener('keydown', event => {
                 if (event.code == "KeyW") this.player.y -= paddleSensitivity;
            else if (event.code == "KeyS") this.player.y += paddleSensitivity;
            else console.info(`Undefined key was pressed: '${event.code}'`)
        })
    }

    physics() {
        this.ai()
        this.ball.physics()

        if (this.ball.edgeX <= 60) {
            this.ball.xVelocity = -this.ball.xVelocity;
            this.ball.yVelocity = -this.ball.yVelocity;
        }
        if (this.ball.edgeX >= this.canvas.width - 90) {
            this.ball.xVelocity = -this.ball.xVelocity;
            this.ball.yVelocity = -this.ball.yVelocity;
        }

        this.render()
    }

    ai = () => {
        if (this.ball.y > this.bot.y) this.bot.y += movementPerSecond / targetFPS;
        if (this.ball.y < this.bot.y) this.bot.y -= movementPerSecond / targetFPS;
    }

    render = () => {
        const c = this.context

        // make full size
        this.canvas.width = window.innerWidth
        this.canvas.height = window.innerHeight

        // background
        c.fillStyle = backgroundColor
        c.fillRect(0, 0, this.canvas.width, this.canvas.height)

        this.player.draw()
        this.bot.draw()
        this.ball.draw()
    }
}

export default GameController